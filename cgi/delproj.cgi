#!/usr/bin/perl
# (c) Petr Baudis <pasky@suse.cz>
# GPLv2

use strict;
use warnings;

use lib qw(/home/repo/repomgr/cgi);
use Git::RepoCGI;

my $repo = Git::RepoCGI->new('Project Removal');
my $name = $repo->sparam('name');

my $proj = $repo->load_project($name);
$proj->{cpwd} = $repo->sparam('cpwd');

$repo->bye("Sorry but you can remove only mirrored projects. "
		. "Pushed projects cannot be removed. "
		. "If you feel this action is necessary, please contact "
		. "the site administrator.")
	unless $proj->{mirror};

$repo->bye("Sorry but this project has forks associated. "
		. "Such projects cannot be removed. "
		. "If you feel this action is necessary, please contact "
		. "the site administrator.")
	if $proj->has_forks;

if ($repo->sparam('y0')) {
	# submitted
	$repo->bye("Authentication failed.  Your money is no good here.")
		unless $proj->authenticate($repo);
	$proj->delete;
	$repo->bye("Project successfuly deleted. Have a nice day.");
}

my $url = $proj->{url};

print <<EOT;
<p>Please confirm that you intend to remove the mirrored project
'$name' ($url) from this site.</p>
<form method="post">
<input type="hidden" name="name" value="$name" />
<p>Admin password: <input type="password" name="cpwd" />
  <sup><a href="pwproj.cgi?name=$name">(forgot password?)</a></sup></p>
<p><input type="submit" name="y0" value="Remove" /></p>
</form>
EOT
