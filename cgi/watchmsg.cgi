#!/usr/bin/perl
# (c) Zachary T Welch <zw@superlucidity.net>
# GPLv2

use strict;
use warnings;

use lib qw(/home/repo/repomgr/cgi);
use Git::RepoCGI;

my $repo = Git::RepoCGI->new('Notify Watchers');
my $project = $repo->sparam('project');
my $branch = $repo->sparam('branch');
my $path = $repo->sparam('path');
my $name = $repo->sparam('name');

# check for submission
my $incomplete = grep { !$repo->sparam($_) } qw( project msg name cpwd go );
$repo->bye('Not yet implemented') unless $incomplete;

print <<EOT
<p>
Using the form on this page, users with push access to '$project.git'
can send messages to some or all registered watchers of a particular
fork, branch, or path.  By leaving all fields in the form blank, the
message will be sent to all project watchers, regardless of the branch
or path that they are watching.
</p>
<!--
<p>
Users can <a href="watchabuse.cgi?project=$project">report abuses</a>
of this messaging system, allowing projects and forks to have repeat
offenders' accounts added to a blacklist automatically.
</p>
-->

<h2>Contact Watchers</h2>

To send a message, specify the intended recipients, the content of the
message to forward, and your authentication credentials.

<h3>Recipients</h3>
<form method="post">
<p>Project: <em>$project</em>
	<input type="hidden" name="project" value="$project" /></p>
<p>Branch: <input type="text" name="branch" value="$branch" /></p>
<p>Path: <input type="text" name="path" value="$path" /></p>
<p>User: <input type="text" name="user" /></p>

<h3>Message Content</h3>
<textarea name="msg"/ rows="10" cols="80"></textarea></p>

<h3>Account Authentication</h3>
If you do not have an account, then <a href="reguser.cgi">register</a> now.
<p>Account: <input type="text" name="name" value="$name" /></p>
<p>Password: <input type="password" name="cpwd" /></p>
<br />
<p><input type="submit" name="go" value="Send" /></p>
</form>
EOT
