#!/usr/bin/perl
# (c) Petr Baudis <pasky@suse.cz>
# GPLv2

use strict;
use warnings;

use lib qw(/home/repo/repomgr/cgi);
use Git::RepoCGI;

my $repo = Git::RepoCGI->new('User Registration');

$repo->bye("<p>Go away, bot.</p>") if $repo->sparam('mail');

if ($repo->sparam('y0')) {
	# submitted, let's see
	# FIXME: racy, do a lock
	my $name = $repo->wparam('name');
	valid_user_name($name)
		and Git::RepoCGI::User::does_exist($name)
		and $repo->err("User with that name already exists.");

	my $user = Git::RepoCGI::User->ghost($name);
	if ($user->cgi_fill($repo)) {
		$user->conjure;
		print <<EOT;
<p>User $name successfuly registered.</p>
<p>
You can now <a href="regproj.cgi?mode=push">start a hosted project</a>
and <a href="regproj.cgi?mode=fork">create forks</a>.  To use them, you
need to <a href="editproj.cgi">grant push access</a> to yourself,
using the project's adminstration password.
</p>
<p>
Of course, other administrators can give you access to an existing
project, but why share their project when <em>you can have your own
fork</em>?  You can have push access to many projects, and little
additional resources will be used by creating new forks.
</p>
<p>Congratulations!  Now, get forking!</p>
EOT
		exit;
	}
}

my $name = $repo->wparam('name');
my $email = $repo->sparam('email');
my $keys = $repo->sparam('keys');
print <<EOT;
<p>
This page allows you to register as a user of this service.
You must register as a user in order to be granted push access
to depot mode projects that you create.
</p>

<h2>User Acount Access</h2>
<p>
SSH is used for pushing (the <tt>git+ssh</tt> protocol), and the
SSH key that you provide in this form will be used to authenticate you.
There is no password; however, we recommend that your private SSH key
be password-protected; use <code>ssh-agent</code> to help your fingers.
</p>
You can find your public key in <tt>~/.ssh/id_rsa.pub</tt> or
<tt>~/.ssh/id_dsa.pub</tt>.  If you do not have one yet, you can
generate it using the <code>ssh-keygen</code> command.
</p>
<p>
You can paste multiple keys in the box below, each on a separate line.
Paste each key <em>including</tt> the <tt>ssh-</tt>whatever prefix and
email-like postfix.
</p>

<h2>E-mail Address</h2>
<p>
We do not share your email contact, but this address may be used to
contact you or confirm your identity shall the need arise.  We also
need to send you an e-mail if you want to update your SSH keys later.
</p>

<h2>Terms of Use</h2>
<p>
By submitting this form, you are confirming that you will push only free
software and no content that would violate any law of Czech Republic.
You also agree to the <a href=/about.html">terms for using the site</a>.
</p>
<p>
Have fun!
</p>

<h3>Register Account</h3>
<form method="post">
<p>Login: <input type="text" name="name" value="$name" /></p>
<p>Email: <input type="text" name="email" value="$email" /></p>
<p>Public SSH key(s): <br />
<textarea name="keys" rows="5" cols="80">$keys</textarea></p>
<p style="display:none">Anti-captcha (leave empty!): <input type="text" name="mail" /></p>
<p><input type="submit" name="y0" value="Register" /></p>
</form>
EOT
