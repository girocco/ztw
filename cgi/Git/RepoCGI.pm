package Git::RepoCGI;

use strict;
use warnings;

### Administrativa

my ( $bin_path, $jail_path, $repomgr_path, $repodata_path, $repo_path );
my ( $site_domain, $group_file, $user_file, $sshkeys_path, $doghouse_path );
BEGIN {
	our $VERSION = '0.1';
	our @ISA = qw(Exporter);
	our @EXPORT = qw(genpwd scrypt html_esc jailed_file
	                 lock_file unlock_file
	                 filedb_atomic_append filedb_atomic_edit
			 proj_get_forkee_name proj_get_forkee_path
			 valid_proj_name valid_user_name valid_email valid_repo_url valid_web_url);

	use CGI qw(:standard :escapeHTML -nosticky);
	use CGI::Util qw(unescape);
	use CGI::Carp qw(fatalsToBrowser);
	use Digest::SHA1 qw(sha1_hex);
	use Git::RepoCGI::Config;

	$bin_path = $Git::RepoCGI::Config::defaults{bin_path};
	$jail_path = $Git::RepoCGI::Config::defaults{jail_path};
	$repomgr_path = $Git::RepoCGI::Config::defaults{repomgr_path};
	$repodata_path = $Git::RepoCGI::Config::defaults{repodata_path};
	$repo_path = $Git::RepoCGI::Config::defaults{repo_path};

	$group_file = $Git::RepoCGI::Config::defaults{group_path};
	$user_file = $Git::RepoCGI::Config::defaults{user_file};
	$sshkeys_path = $Git::RepoCGI::Config::defaults{sshkeys_path};
	$doghouse_path = $Git::RepoCGI::Config::defaults{doghouse_path};

	$site_domain = $Git::RepoCGI::Config::defaults{site_domain};

	$ENV{PATH} = $bin_path . ':' . $ENV{PATH};
}


### RepoCGI object

sub new {
	my $class = shift;
	my ($heading) = @_;
	my $repo = {};

	$repo->{cgi} = CGI->new;

	print $repo->{cgi}->header(-type=>'text/html', -charset => 'utf-8');

	print <<EOT;
<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">

<head>
<title>$site_domain :: $heading</title>
<link rel="stylesheet" type="text/css" href="/gitweb.css"/>
<link rel="shortcut icon" href="/git-favicon.png" type="image/png"/>
</head>

<body>

<div class="page_header">
<a href="http://git.or.cz/" title="Git homepage"><img src="/git-logo.png" width="72" height="27" alt="git" style="float:right; border-width:0px;"/></a>
<a href="/">$site_domain</a> / administration / $heading
</div>

EOT

	bless $repo, $class;
}

sub DESTROY {
	my $self = shift;
	my $cgi = $self->cgi;
	my $cginame = $cgi->url(-absolute => 1);
	$cginame =~ s#^/m/##;
	if ($cginame =~ /^[a-zA-Z0-9_.\/-]+\.cgi$/) {
		print <<EOT;
<div align="right">
<a href="http://$site_domain/w/repo.git?a=blob;f=cgi/$cginame">(view source)</a>
</div>
EOT
	}
	print <<EOT;
</body>
</html>
EOT
}

sub cgi {
	my $self = shift;
	$self->{cgi};
}

sub bye {
	my $self = shift;
	print "<p>", @_, "</p>\n" if @_;
	exit
}

sub err {
	my $self = shift;
	print "<p style=\"color: red\">@_</p>\n";
	$self->{err}++;
}

sub err_check {
	my $self = shift;
	my $err = $self->{err};
	$err and print "<p style=\"font-weight: bold\">Operation aborted due to $err errors.</p>\n";
	$err;
}

sub sparam {
	my ( $self, $param ) = @_;
	$self->{cgi}->param($param) || '';
}
sub wparam {
	my $self = shift;
	my $val = $self->sparam(@_);
	$val =~ s/^\s*(.*?)\s*$/$1/;
	$val;
}


### Random utility functions

sub genpwd {
	# FLUFFY!
	substr(crypt(rand, rand), 2);
}

sub scrypt {
	my ($pwd) = @_;
	crypt($pwd, join ('', ('.', '/', 2..9, 'A'..'Z', 'a'..'z')[rand 64, rand 64]));
}

sub html_esc {
	my ($str) = @_;
	$str =~ s/&/&amp;/g;
	$str =~ s/</&lt;/g; $str =~ s/>/&gt;/g;
	$str =~ s/"/&quot;/g;
	$str;
}

sub site_domain {
	return $site_domain;
}

sub jailed_file {
	my ($filename) = @_;
	"$jail_path/$filename";
}

sub lock_file {
	my ($path) = @_;

	$path .= '.lock';

	use Errno qw(EEXIST);
	use Fcntl qw(O_WRONLY O_CREAT O_EXCL);
	use IO::Handle;
	my $handle = new IO::Handle;

	unless (sysopen($handle, $path, O_WRONLY|O_CREAT|O_EXCL)) {
		my $cnt = 0;
		while (not sysopen($handle, $path, O_WRONLY|O_CREAT|O_EXCL)) {
			($! == EEXIST) or die "$path open failed: $!";
			($cnt++ < 16) or die "$path open failed: cannot open lockfile";
			sleep(1);
		}
	}
	# XXX: filedb-specific
	chmod 0664, $path or die "$path g+w failed: $!";

	$handle;
}

sub unlock_file {
	my ($path) = @_;

	rename "$path.lock", $path or die "$path unlock failed: $!";
}

sub filedb_atomic_append {
	my ($file, $line) = @_;
	my $id = 65536;

	open my $src, $file or die "$file open for reading failed: $!";
	my $dst = lock_file($file);

	while (<$src>) {
		my $aid = (split /:/)[2];
		$id = $aid + 1 if ($aid >= $id);

		print $dst $_ or die "$file(l) write failed: $!";
	}

	$line =~ s/\\i/$id/g;
	print $dst "$line\n" or die "$file(l) write failed: $!";

	close $dst or die "$file(l) close failed: $!";
	close $src;

	unlock_file($file);

	$id;
}

sub filedb_atomic_edit {
	my ($file, $fn) = @_;

	open my $src, $file or die "$file open for reading failed: $!";
	my $dst = lock_file($file);

	while (<$src>) {
		print $dst $fn->($_) or die "$file(l) write failed: $!";
	}

	close $dst or die "$file(l) close failed: $!";
	close $src;

	unlock_file($file);
}

sub proj_get_forkee_name {
	$_ = $_[0];
	(m#^(.*)/.*?$#)[0];
}
sub proj_get_forkee_path {
	my $forkee = $repo_path . proj_get_forkee_name($_[0]).'.git';
	-d $forkee ? $forkee : '';
}
sub valid_proj_name {
	$_ = $_[0];
	(not m#/# or -d proj_get_forkee_path($_)) # will also catch ^/
		and (not m#\./#)
		and (not m#/$#)
		and m#^[a-zA-Z0-9+./_-]+$#;
}
sub valid_user_name {
	$_ = $_[0];
	/^[a-zA-Z0-9+._-]+$/;
}
sub valid_email {
	$_ = $_[0];
	/^[a-zA-Z0-9+._-]+@[a-zA-Z0-9-.]+$/;
}
sub valid_web_url {
	$_ = $_[0];
	/^http:\/\/[a-zA-Z0-9-.]+(\/[_\%a-zA-Z0-9.\/~-]*)?(#[a-zA-Z0-9._-]+)?$/;
}
sub valid_repo_url {
	$_ = $_[0];
	/^http:\/\/[a-zA-Z0-9-.]+(\/[_\%a-zA-Z0-9.\/~-]*)?$/ or
	 /^git:\/\/[a-zA-Z0-9-.]+(\/[_\%a-zA-Z0-9.\/~-]*)?$/;
}

sub load_project {
	my ( $self, $name ) = @_;

	$self->bye("I need the project name as an argument now.")
		unless $name;

	$self->bye("Invalid project name. Go away, sorcerer.")
		unless valid_proj_name($name);

	$self->bye("Sorry but this project does not exist. "
			. "Now, how did you <em>get</em> here?!")
		unless Git::RepoCGI::Project::does_exist($name);

	$self->bye("Sorry but your project has not finished mirroring yet.  "
			. "If this process takes excessive time, "
			. "please inform the site administrator.")
		unless Git::RepoCGI::Project::available($name);

	my $proj = Git::RepoCGI::Project->load($name);
	die "not found project $name, that's really weird!" unless $proj;
	$proj
}

sub load_user {
	my ( $self, $name ) = @_;
	$self->bye("User '$name' is not registered.")
		unless valid_user_name($name)
			&& Git::RepoCGI::User::does_exist($name);

	my $user = Git::RepoCGI::User->load($name);
	$self->bye("User '$name' failed to load") unless $user;
	return $user;
}

### Project object

package Git::RepoCGI::Project;

BEGIN { use Git::RepoCGI; }

sub _mkdir_forkees {
	my $self = shift;
	my @pelems = split('/', $self->{name});
	pop @pelems; # do not create dir for the project itself
	my $path = $self->{base_path};
	foreach my $pelem (@pelems) {
		$path .= "/$pelem";
		(-d "$path") or mkdir $path or die "mkdir $path: $!";
		chmod 0775, $path; # ok if fails (dir may already exist and be owned by someone else)
	}
}

sub has_forks {
	my $self = shift;
	glob($self->{base_path} . '/' . $self->{name} . '/*');
}

sub push_url {
	my $self = shift;
	my $name = $self->{name} . ".git";
	return "git+ssh://" . $site_domain . $self->{base_name} . "/" . $name;
}

our %propmap = (
	url => 'base_url',
	email => 'owner',
	desc => 'description',
	README => 'README.html',
	hp => 'homepage',
);

sub _property_path {
	my $self = shift;
	my ($name) = @_;
	$self->{path}.'/'.$name;
}

sub _property_fget {
	my $self = shift;
	my ($name) = @_;
	$propmap{$name} or die "unknown property: $name";
	open P, $self->_property_path($propmap{$name}) or return undef;
	my @value = <P>;
	close P;
	my $value = join('', @value); chomp $value;
	$value;
}

sub _property_fput {
	my $self = shift;
	my ($name, $value) = @_;
	$propmap{$name} or die "unknown property: $name";

	my $P = lock_file($self->_property_path($propmap{$name}));
	$value ne '' and print $P "$value\n";
	close $P;
	unlock_file($self->_property_path($propmap{$name}));
}

sub _properties_load {
	my $self = shift;
	foreach my $prop (keys %propmap) {
		$self->{$prop} = $self->_property_fget($prop);
	}
}

sub _properties_save {
	my $self = shift;
	foreach my $prop (keys %propmap) {
		$self->_property_fput($prop, $self->{$prop});
	}
}

sub _nofetch_path {
	my $self = shift;
	$self->_property_path('.nofetch');
}

sub _nofetch {
	my $self = shift;
	my ($nofetch) = @_;
	my $np = $self->_nofetch_path;
	if ($nofetch) {
		open X, '>'.$np or die "nofetch failed: $!";
		close X;
	} else {
		unlink $np or die "yesfetch failed: $!";
	}
}

sub _alternates_setup {
	my $self = shift;
	return unless $self->{name} =~ m#/#;
	my $forkee_name = proj_get_forkee_name($self->{name});
	my $forkee_path = proj_get_forkee_path($self->{name});
	return unless -d $forkee_path;
	mkdir $self->{path}.'/refs'; chmod 0775, $self->{path}.'/refs';
	mkdir $self->{path}.'/objects'; chmod 0775, $self->{path}.'/objects';
	mkdir $self->{path}.'/objects/info'; chmod 0775, $self->{path}.'/objects/info';

	# We set up both alternates and http_alternates since we cannot use
	# relative path in alternates - that doesn't work recursively.

	my $filename = $self->{path}.'/objects/info/alternates';
	open X, '>'.$filename or die "alternates failed: $!";
	print X "$forkee_path/objects\n";
	close X;
	chmod 0664, $filename or warn "cannot chmod $filename: $!";

	$filename = $self->{path}.'/objects/info/http-alternates';
	open X, '>'.$filename or die "http-alternates failed: $!";
	my $upfork = $forkee_name;
	do { print X "/r/$upfork.git/objects\n"; } while ($upfork =~ s#/?.+?$## and $upfork);
	close X;
	chmod 0664, $filename or warn "cannot chmod $filename: $!";

	symlink "$forkee_path/refs", $self->{path}.'/refs/forkee';
}

sub _ctags_setup {
	my $self = shift;
	mkdir $self->{path}.'/ctags'; chmod 0775, $self->{path}.'/ctags';
}

sub _group_add {
	my $self = shift;
	my ($xtra) = @_;
	$xtra .= join(',', @{$self->{users}});
	filedb_atomic_append(jailed_file($group_file),
		join(':', $self->{name}, $self->{crypt}, '\i', $xtra));
}

sub _group_update {
	my $self = shift;
	my $xtra = join(',', @{$self->{users}});
	filedb_atomic_edit(jailed_file($group_file),
		sub {
			$_ = $_[0];
			chomp;
			if ($self->{name} eq (split /:/)[0]) {
				# preserve readonly flag
				s/::([^:]*)$/:$1/ and $xtra = ":$xtra";
				return join(':', $self->{name}, $self->{crypt}, $self->{gid}, $xtra)."\n";
			} else {
				return "$_\n";
			}
		}
	);
}

sub _group_remove {
	my $self = shift;
	filedb_atomic_edit(jailed_file($group_file),
		sub {
			$self->{name} ne (split /:/)[0] and return $_;
		}
	);
}

sub _hook_path {
	my $self = shift;
	my ($name) = @_;
	$self->{path}.'/hooks/'.$name;
}

sub _hook_install {
	my $self = shift;
	my ($name) = @_;
	open SRC, "$repomgr_path/$name-hook" or die "cannot open hook $name: $!";
	open DST, '>'.$self->_hook_path($name) or die "cannot open hook $name for writing: $!";
	while (<SRC>) { print DST $_; }
	close DST;
	close SRC;
	chmod 0775, $self->_hook_path($name) or die "cannot chmod hook $name: $!";
}

sub _hooks_install {
	my $self = shift;
	foreach my $hook ('update') {
		$self->_hook_install($hook);
	}
}

# private constructor, do not use
sub _new {
	my $class = shift;
	my ($name, $base_path, $path) = @_;
	valid_proj_name($name) or die "refusing to create project with invalid name ($name)!";
	$path ||= "$base_path/$name.git";
	my $proj = { name => $name, base_path => $base_path, path => $path };

	bless $proj, $class;
}

# public constructor #0
# creates a virtual project not connected to disk image
# you can conjure() it later to disk
sub ghost {
	my $class = shift;
	my ($name, $mirror) = @_;

	my $path = $mirror ? "$repodata_path/to-clone" : $repo_path;
	my $repo = "$path/$name";
	$repo .= '.git' unless $mirror;

	my $self = $class->_new($name, $path, $repo);
	$self->{users} = [];
	$self->{mirror} = $mirror;
	$self;
}

# public constructor #1
sub load {
	my $class = shift;
	my ($name) = @_;

	open F, jailed_file($group_file) or die "project load failed: $!";
	while (<F>) {
		chomp;
		@_ = split /:+/;
		next unless (shift eq $name);

		my $self = $class->_new($name, $repo_path);
		(-d $self->{path}) or die "invalid path (".$self->{path}.") for project ".$self->{name};

		my $ulist;
		($self->{crypt}, $self->{gid}, $ulist) = @_;
		$ulist ||= '';
		$self->{users} = [split /,/, $ulist];
		$self->{mirror} = ! -e $self->_nofetch_path;
		$self->{ccrypt} = $self->{crypt};

		$self->_properties_load;
		return $self;
	}
	close F;
	undef;
}

# $proj may not be in sane state if this returns false!
sub cgi_fill {
	my $self = shift;
	my ($repo) = @_;
	my $cgi = $repo->cgi;

	my $pwd = $cgi->param('pwd');
	if ($pwd ne '' or not $self->{crypt}) {
		$self->{crypt} = scrypt($pwd);
	}

	if ($cgi->param('pwd2') and $pwd ne $cgi->param('pwd2')) {
	 	$repo->err("Our high-paid security consultants have determined that the admin passwords you have entered do not match each other.");
	}

	$self->{cpwd} = $cgi->param('cpwd');

	$self->{email} = $repo->wparam('email');
	valid_email($self->{email})
		or $repo->err("Your email sure looks weird...?");

	$self->{url} = $repo->wparam('url');
	if ($self->{url}) {
		valid_repo_url($self->{url})
			or $repo->err("Invalid URL. Note that only HTTP and Git protocol is supported. If the URL contains funny characters, contact me.");
	}

	$self->{desc} = $repo->wparam('desc');
	length($self->{desc}) <= 1024
		or $repo->err("<b>Short</b> description length > 1kb!");

	$self->{README} = $repo->wparam('README');
	length($self->{README}) <= 8192
		or $repo->err("README length > 8kb!");

	$self->{hp} = $repo->wparam('hp');
	if ($self->{hp}) {
		valid_web_url($self->{hp})
			or $repo->err("Invalid homepage URL. Note that only HTTP protocol is supported. If the URL contains funny characters, contact me.");
	}

	# FIXME: Permit only existing users
	$self->{users} = [grep { valid_user_name($_) } $cgi->param('user')];

	not $repo->err_check;
}

sub form_defaults {
	my $self = shift;
	(
		name => $self->{name},
		email => $self->{email},
		url => $self->{url},
		desc => html_esc($self->{desc}),
		README => html_esc($self->{README}),
		hp => $self->{hp},
		users => $self->{users},
	);
}

sub authenticate {
	my $self = shift;
	my ($repo) = @_;

	$self->{ccrypt} or die "Can't authenticate against a project with no password";
	$self->{cpwd} or $repo->err("No password entered.");
	unless ($self->{ccrypt} eq crypt($self->{cpwd}, $self->{ccrypt})) {
		$repo->err("Your admin password does not match!");
		return 0;
	}
	return 1;
}

sub premirror {
	my $self = shift;

	$self->_mkdir_forkees;
	mkdir $self->{path} or die "mkdir failed: $!";
	chmod 0775, $self->{path} or die "chmod failed: $!";
	$self->_properties_save;
	$self->_alternates_setup;
	$self->_ctags_setup;
	$self->_group_add(':');
}

sub conjure {
	my $self = shift;

	$self->_mkdir_forkees;
	system('cg-admin-setuprepo', '-g', 'repo', $self->{path}) == 0
		or die "cg-admin-setuprepo failed: $?";
	system('git', '--git-dir='.$self->{path}, 'config', 'receive.denyNonFastforwards', 'false');
	$self->_nofetch(1);
	$self->_properties_save;
	$self->_alternates_setup;
	$self->_ctags_setup;
	$self->_group_add;
	$self->_hooks_install;
}

sub update {
	my $self = shift;

	$self->_properties_save;
	$self->_group_update;
}

sub update_password {
	my $self = shift;
	my ($pwd) = @_;

	$self->{crypt} = scrypt($pwd);
	$self->_group_update;
}

# You can explicitly do this just on a ghost() repository too.
sub delete {
	my $self = shift;

	if (-d $self->{path}) {
		system('rm', '-r', $self->{path}) == 0
			or die "rm -r failed: $?";
	}
	$self->_group_remove;
}

# static method
sub does_exist {
	my ($name) = @_;
	valid_proj_name($name) or die "tried to query for project with invalid name $name!";
	(available($name)
		or -d "$repodata_path/cloning/$name"
		or -d "$repodata_path/to-clone/$name");
}
sub available {
	my ($name) = @_;
	valid_proj_name($name) or die "tried to query for project with invalid name $name!";
	(-d "$repo_path/$name.git");
}

# watchdog method
sub load_watchdogs {
	my ( $self, %opts ) = @_;
	return Git::RepoCGI::Watch::load_watchdogs(%opts,
		type=> 'project', name => $self->{name});
}

### User object

package Git::RepoCGI::User;

BEGIN { use Git::RepoCGI; }

sub _passwd_text {
	my $self = shift;
	join(':', $self->{name}, $self->{crypt}, $self->{uid}, 65534, $self->{email}, '/', '/bin/git-shell')
}

sub _user_update {
	my $self = shift;
	my $xtra = join(',', @{$self->{users}});
	filedb_atomic_edit(jailed_file($user_file),
		sub {
			$_ = $_[0];
			chomp;
			if ($self->{name} eq (split /:/)[0]) {
				return $self->_passwd_text . "\n";
			} else {
				return "$_\n";
			}
		}
	);
}

sub _passwd_add {
	my $self = shift;
	$self->{uid} = '\i';
	filedb_atomic_append(jailed_file($user_file), $self->_passwd_text);
}

sub update_password {
	my $self = shift;
	my ($pwd) = @_;

	$self->{crypt} = scrypt($pwd);
	$self->_user_update;
}
sub authenticate {
	my $self = shift;
	my ($repo) = @_;

	$self->{ccrypt} or die "Can't authenticate against a user with no password";
	$self->{cpwd} or $repo->err("No password entered.");
	unless ($self->{ccrypt} eq crypt($self->{cpwd}, $self->{ccrypt})) {
		$repo->err("Your admin password does not match!");
		return 0;
	}
	return 1;
}

sub _sshkey_path {
	my $self = shift;
	$sshkeys_path . '/' . $self->{name};
}

sub _sshkey_load {
	my $self = shift;
	open F, "<".jailed_file($self->_sshkey_path) or die "sshkey load failed: $!";
	my @keys;
	my $auth;
	while (<F>) {
		chomp;
		if (/^ssh-(?:dss|rsa) /) {
			push @keys, $_;
		} elsif (/^# REPOAUTH ([0-9a-f]+) (\d+)/) {
			my $expire = $2;
			$auth = $1 unless (time >= $expire);
		}
	}
	close F;
	my $keys = join('', @keys); chomp $keys;
	($keys, $auth);
}

sub _sshkey_save {
	my $self = shift;
	open F, ">".jailed_file($self->_sshkey_path) or die "sshkey failed: $!";
	if (defined($self->{auth}) && $self->{auth}) {
		my $expire = time + 24 * 3600;
		print F "# REPOAUTH $self->{auth} $expire\n";
	}
	print F $self->{keys}."\n";
	close F;
	chmod 0664, jailed_file($self->_sshkey_path);
}

# private constructor, do not use
sub _new {
	my $class = shift;
	my ($name) = @_;
	valid_user_name($name) or die "refusing to create user with invalid name ($name)!";
	my $proj = { name => $name };

	bless $proj, $class;
}

# public constructor #0
# creates a virtual user not connected to disk record
# you can conjure() it later to disk
sub ghost {
	my $class = shift;
	my ($name) = @_;
	my $self = $class->_new($name);
	$self;
}

# public constructor #1
sub load {
	my $class = shift;
	my ($name) = @_;

	open F, jailed_file($user_file) or die "user load failed: $!";
	while (<F>) {
		chomp;
		@_ = split /:+/;
		next unless (shift eq $name);

		my $self = $class->_new($name);

		($self->{crypt}, $self->{uid}, undef, $self->{email}) = @_;
		($self->{keys}, $self->{auth}) = $self->_sshkey_load;
		$self->{ccrypt} = $self->{crypt};

		return $self;
	}
	close F;
	undef;
}

# $user may not be in sane state if this returns false!
sub cgi_fill {
	my $self = shift;
	my ($repo) = @_;

	$self->{name} = $repo->wparam('name');
	valid_user_name($self->{name})
		or $repo->err("Name contains invalid characters.");

	$self->{email} = $repo->wparam('email');
	valid_email($self->{email})
		or $repo->err("Your email sure looks weird...?");

	my $pwd = $repo->sparam('pwd');
	if ($pwd ne '' or not $self->{crypt}) {
		$self->{crypt} = scrypt($pwd);
	}

	if ($repo->sparam('pwd2') and $pwd ne $repo->sparam('pwd2')) {
	 	$repo->err("Our high-paid security consultants have determined that the passwords you have entered do not match each other.");
	}

	$self->{cpwd} = $repo->sparam('cpwd');

	$self->keys_fill($repo);
}

sub keys_fill {
	my $self = shift;
	my ($repo) = @_;
	my $cgi = $repo->cgi;

	$self->{keys} = $cgi->param('keys');
	length($self->{keys}) <= 4096
		or $repo->err("The list of keys is more than 4kb. Do you really need that much?");
	foreach (split /\r?\n/, $self->{keys}) {
		/^ssh-(?:dss|rsa) .* \S+@\S+$/ or $repo->err("Your ssh key (\"$_\") appears to have invalid format (does not start by ssh-dss|rsa or does not end with @-identifier) - maybe your browser has split a single key to multiple lines?");
	}

	not $repo->err_check;
}

sub keys_save {
	my $self = shift;

	$self->_sshkey_save;
}

sub gen_auth {
	my $self = shift;

	$self->{auth} = Digest::SHA1::sha1_hex(time . $$ . rand() . $self->{keys});
	$self->_sshkey_save;
	$self->{auth};
}

sub del_auth {
	my $self = shift;

	delete $self->{auth};
}

sub conjure {
	my $self = shift;

	$self->_passwd_add;
	$self->_sshkey_save;
}

# static method
sub does_exist {
	my ($name) = @_;
	valid_user_name($name) or die "tried to query for user with invalid name $name!";
	(-e jailed_file("$sshkeys_path/$name"));
}
sub available {
	does_exist(@_);
}

# watchdog method
sub load_watchdogs {
	my ( $self, %opts ) = @_;
	return Git::RepoCGI::Watch::load_watchdogs(%opts,
		type=> 'user', name => $self->{name});
}

### Watch object

package Git::RepoCGI::Watch;

BEGIN { use Git::RepoCGI; }

# public constructor #1
sub new {
	my ( $class, $user, $project, $branch, $path ) = @_;
	my $self = { user => $user,
		project => $project, branch => $branch, path => $path };
	bless $self, $class;
}

sub _try_match {
	my ( $self, $opts, $key ) = @_;
	# if watchdog does not specify it, then it matches
	return 1 unless $self->{$key};
	my $val = $self->{$key};
	return $opts->{$key} =~ qr/$val/;
}

sub match {
	my ( $self, %opts ) = @_;
	my @match_keys = qw( project branch path user );
	return ! grep { !$self->_try_match(\%opts, $_) } @match_keys;
}

sub _find_keys {
	my ( $self, $key, $opts ) = @_;
	for ($opts->{type}) {
	/^project$/ and do { return ( $key, $opts->{name} ) };
	/^user$/ and do { return ( $opts->{name}, $key ) };
	die "unknown type of watchdog: $_";
	}
}
# constructor: loads all watchdogs
#   Valid options:
#	type => 'project' or 'user'
#	name => name of the project or user
#	user, project, branch, path => regular expression filters
#   Returns a list of watchdog objects
sub load {
	my ( %opts ) = @_;
	my $doghouse = "$doghouse_path/$opts{type}/$opts{name}";
	return () unless -f $doghouse;

	open F, jailed_file($doghouse) or die "loading '$doghouse' failed: $!";
	my @dogs = ();
	while (<F>) {
		chomp;
		my ( $key, $branch, $path ) = @_ = split /:+/;
		my ( $user, $project ) = _find_keys($key, \%opts);

		my $dog = __PACKAGE__->new($user, $project, $branch, $path);
		next unless $dog->match(%opts);

		for ($opts{type}) {
		/^user$/ and do {
			my $proj = Git::RepoCGI::Project->_new($project, $repo_path);
			die "invalid watchdog for user $user: "
					. "project '$project' is missing."
				unless -d $proj->{path};
		};
		/^project$/ and do {
			die "invalid watchdog for project $project: "
					. "user '$user' is not valid."
				unless valid_user_name($user);
		};
		die "unknown type of watchdog: $_";
		}
		push @dogs, $dog;
	}
	close F;
	return @dogs;
}

sub authenticate {
	my ( $self, $repo ) = @_;

	my $cpwd = $repo->sparam('cpwd');
	my $user = $repo->load_user($self->{user});
	$user->{cpwd} = $cpwd;
	return 1 if $user->authenticate($repo);

	my $proj = $repo->load_project($self->{project});
	$proj->{cpwd} = $cpwd;
	$repo->bye("Authentication failed") unless $proj->authenticate($repo);

}
sub _watch_file {
	my ( $self, $type ) = @_;
	my $key = $self->{$type};
	return jailed_file("$doghouse_path/$type/$key");
}

sub add {
	my ( $self ) = @_;

	my ( $user, $project, $branch, $path ) =
			map { $self->{$_} } qw( user project branch path );
	filedb_atomic_append($self->_watch_file('user'),
		join(':', $project, $branch, $path));
	filedb_atomic_append($self->_watch_file('project'),
		join(':', $user, $branch, $path));
}
sub remove {
	my ( $self ) = @_;
	my $remove_args = sub {
		my $key = $_[0];
		my $e = sub { $self->{$key} ne (split /:/)[0] and return $_ };
		( $self->_watch_file($key), $e ) };
	filedb_atomic_edit($remove_args->('user'));
	filedb_atomic_edit($remove_args->('project'));
}

1;
