#!/usr/bin/perl
# (c) Petr Baudis <pasky@suse.cz>
# GPLv2

use strict;
use warnings;

use lib qw(/home/repo/repomgr/cgi);
use Git::RepoCGI;

my $repo = Git::RepoCGI->new('Project Settings');
my $name = $repo->sparam('name');

my $proj = $repo->load_project($name);

if ($repo->sparam('email')) {
	$repo->bye("Form failed. Try again.") unless $proj->cgi_fill($repo);
	$repo->bye("Authentication failed.") unless $proj->authenticate($repo);
	my $okay = $proj->update ? "" : ' <span style="color:red">not</span>';
	print "<p><em>The settings were$okay updated.</em></p>\n";
}

# $proj may be insane now but that's actually good for us since we'll let the
# user fix the invalid values she entered
my %h = $proj->form_defaults;

print <<EOT;
<form method="post">
<p>Project name: <a href="/w/$h{name}.git">$h{name}</a>.git
	<input type="hidden" name="name" value="$h{name}" /></p>
<h3>Contact/Description </h3>
<p>E-mail contact: <input type="text" name="email" value="$h{email}" /></p>
<p>Repository URL: <input type="text" name="url" value="$h{url}" /></p>
<p>Description: <input type="text" name="desc" value="$h{desc}" /></p>
<p>Homepage URL: <input type="text" name="hp" value="$h{hp}" /></p>
<p>README (HTML, lt 8kb): <br />
	<textarea name="README" rows="5" cols="80">$h{README}</textarea></p>
EOT

if ($proj->{mirror}) {
	print <<EOT;
<h3>Mirror Settings</h3>
<p>
If no forks have been created, then you can
<a href="delproj.cgi?name=$h{name}">remove this project</a> from the site.
</p>
<p>
Since users cannot push changes to this project, the user access
list has no practical effect for this type of project.
</p>
EOT
	exit
}

my $site_repo_url = 'git://' . $repo->site_domain . '/repo.git';
print <<EOT;
<h3>Users</h3>
<p>
Currently, you can add access for only one user at a time.
If this will be a limitation for you, you may create a patch for
<a href="$site_repo_url">the site</a> to improve this functionality,
and it will be incorporated in a future revision.
</p>
EOT

print <<EOT unless grep { $_ eq 'mob' } @{$h{users}};
<p>
<em>Please consider adding the <tt>mob</tt> user to provide
<a href=\"/mob.html\">anonymous push access</a> to the mob branch
of your project.</em>
</p>
EOT

print "<p>Users:</p>\n<ul>\n";
print <<EOT foreach @{$h{users}};
<li><input type="checkbox" name="user" value="$_" checked="1" />$_</li>
EOT

print <<EOT;
<li>Add user: <input type="text" name="user" /></li>
</ul>

<h3>Enter Admin Password</h3>
<p>Admin password: <input type="password" name="cpwd" />
	<sup><a href="pwproj.cgi?name=$name">(forgot password?)</a></sup></p>
<p>New password: <input type="password" name="pwd" />
	(leave empty to keep it at the current value)</p>
<p>Verify password: <input type="password" name="pwd2" /></p>
<br/>
<p><input type="submit" name="y0" value="Change" /></p>
</form>
EOT
