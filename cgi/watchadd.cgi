#!/usr/bin/perl
# (c) Zachary T Welch <zw@superlucidity.net>
# GPLv2

use strict;
use warnings;

use lib qw(/home/repo/repomgr/cgi);
use Git::RepoCGI;

my $repo = Git::RepoCGI->new('Create Watchdog');
my $project = $repo->sparam('project');
my $branch = $repo->sparam('branch');
my $path = $repo->sparam('path');
my $user = $repo->sparam('user');

# check for submission
if ($project && $user && $repo->sparam('go')) {
	my $watch = Git::RepoCGI::Watch->new($user, $project, $branch, $path);
	$watch->add() or $repo->bye("Unable to create watchdog: $!");
	$repo->bye("Watchdog created successfully.");
}

print <<EOT

<p>
Using the form on this page, you may register a <i>watchdog</i> that
will monitor a project or fork for any changes.  When activity occurs,
you will be sent a notification of the changes via e-mail.  You can set
watchdogs to trigger for any change in the project, or you can restrict
them to monitor changes that occur on a specific branch or path in the
repository tree.
</p>
<p>
<p>
Once created, you can <a href="watchedit.cgi">access your watchdogs</a>
to view, update, or delete them.  Users with push access to a project
can <a href="watchspam.cgi?project=$project&branch=$branch&path=$path">send
messages</a> to registered watchers of a particular fork, branch, or path.
</p>

<h2>Create a Watchdog</h2>
<form method="post">
<p>Project: <input type="text" name="project" value="$project" /></p>
<p>Branch: <input type="text" name="branch" value="$branch" />
	<sup>(optional)</sup></p>
<p>Path: <input type="text" name="path" value="$path" />
	<sup>(optional)</sup></p>
<br>
<p>Account: <input type="text" name="user" value="$user" /></p>
<p>Password: <input type="password" name="cpwd" /></p>
<p><input type="submit" name="go" value="Watch" /></p>
</form>
EOT
