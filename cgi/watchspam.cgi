#!/usr/bin/perl
# (c) Zachary T Welch <zw@superlucidity.net>
# GPLv2

use strict;
use warnings;

use lib qw(/home/repo/repomgr/cgi);
use Git::RepoCGI;

my $repo = Git::RepoCGI->new('Report User');
my $project = $repo->sparam('project');

# check for submission
my $incomplete = grep { ! $repo->sparam($_) } qw( user name cpwd go );
$repo->bye('Not yet implemented') unless $incomplete;

print <<EOT
<p>
Using the form on this page, users watching '$project.git' can report
absusive messages sent to them by users using the 'contact' feature.
This allows projects and forks to track repeat or egregious offenders
and add their accounts to a blacklist automatically.
</p>
<p>
In addition, message will be sent to the site administrator and the
contact address listed for the project listed (if provided).  This will
ensure the administrators can take prompt action when required.
</p>

<h3>Report Details</h3>

<form method="post">
<p>Project: <input type="text" name="project" value="$project" />
	<sup>(optional)</sup></p>
<p>Account: <input type="text" name="user" /></p>
<h3>Reason for Report</h3>
<p>
Please provide some context and a description of the abuse for the
administrators, but the automated system often will work magic without
manual intervention.  When it comes to just vengance, the power of the
mob works effectively and efficiently.
</p>
<textarea cols="80" rows="5">
</textarea>
</p>

<h3>Account Authentication</h3>
<p>Account: <input type="text" name="name" /></p>
<p>Password: <input type="password" name="cpwd" /></p>
<br />
<p><input type="submit" name="go" value="Report" /></p>
</form>
EOT
