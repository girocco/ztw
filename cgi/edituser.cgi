#!/usr/bin/perl
# (c) Petr Baudis <pasky@suse.cz>
# (c) Jan Krueger <jk@jk.gs>
# GPLv2

use strict;
use warnings;

use lib qw(/home/repo/repomgr/cgi);
use Git::RepoCGI;

my $repo = Git::RepoCGI->new('User SSH Key Update');

print "<p>Go away, bot.</p>" if $repo->sparam('mail');

sub _auth_form {
	my ( $name, $submit, $fields, $auth ) = @_;
	$fields = '' unless $fields;
	my $authtag = ($auth ?
		qq(<input type="hidden" name="auth" value="$auth" />) :
		qq(<p>Authorization code: <input name="auth" size="40" /></p>));
	print <<EOT;

<form method="post">
<input type="hidden" name="name" value="$name">
$authtag
$fields<p><input type="submit" value="$submit" /></p>
</form>
EOT
	exit
}

sub _keys_field {
	my $keys = shift;
	my $cols = 5;
	my @lines = split(/\n/, $keys);
	my $lines = scalar(@lines);
	$cols = $lines + 2 if $lines > $cols;
	my $fields = <<EOT;
<p>Public SSH key(s): <br />
	<textarea name="keys" cols="80" rows="$cols">$keys</textarea></p>
EOT
}

if ($repo->sparam('name')) {
	# submitted, let's see
	# FIXME: racy, do a lock
	my $name = $repo->wparam('name');
	my $user = $repo->load_user($name);

	if (!$repo->sparam('auth')) {
		my $auth = $user->gen_auth;

		# Send auth mail
		my $site_domain = $Git::RepoCGI::Config::defaults{site_domain};
		open(MAIL, '|-', '/usr/bin/mail', '-s',
			"[$site_domain] Account update authorization",
			$user->{email}) or
			die "Sorry, could not send authorization code: $!";
		print MAIL <<EOT;
Hello,

you have requested that an authorization code be sent to you for updating your
account's SSH keys. If you don't want to actually update your SSH keys, just
ignore this e-mail. Otherwise, use this code within 24 hours:

$auth

Should you run into any problems, please let me know.

Thanks for using $site_domain!
EOT
		close MAIL;

		print <<EOT;
<p>You should receive an e-mail shortly containing your authorization
code.  Please enter this code below to update your SSH keys.  This code
will expire in 24 hours or once you have used it.</p>
EOT
		_auth_form($name, "'Login'");
	} else {
		my $gen_url = "<a href=\"edituser.cgi?name=$name\">"
				. "generate a new authorization code</a>";
		$repo->bye("You must $gen_url.") unless $user->{auth};

		my $keys = $repo->sparam('keys');
		my $fields = '';
		$fields = _keys_field($keys) if $keys;

		my $auth = $repo->wparam('auth');
		if ($auth ne $user->{auth}) {
			print <<EOT;
<p>
Invalid authorization code, but you may re-enter it.  If the code still
does not work, you may $gen_url.
</p>
EOT
			_auth_form($name, "'Login'", $keys ? $fields : '');
		}

		# Auth valid, keys given -> save
		if ($keys) {
			$user->keys_fill($repo);
			$user->del_auth;
			$user->keys_save;
			$repo->bye("Your SSH keys have been updated.");
		}

		# Otherwise pre-fill keys
		$keys = $user->{keys};
		$fields = _key_fields($user->{keys});

		print <<EOT;
<p>Authorization code validated (for now).</p>
<p>You can paste multiple keys in the box below, each on a separate
line.  Paste each key <em>including</em> the <tt>ssh-</tt>whatever
prefix and email-like postfix.</p>
EOT
		_auth_form($name, "Update keys", $fields, $auth);
	}
}

print <<EOT;
<p>Here you can update the public SSH keys associated with your user account. These keys are required for you to push to projects.</p>
<p>SSH is used for pushing (the <tt>git+ssh</tt> protocol), your SSH key authenticates you -
there is no password (though we recommend that your SSH key is password-protected; use <code>ssh-agent</code> to help your fingers).
You can find your public key in <tt>~/.ssh/id_rsa.pub</tt> or <tt>~/.ssh/id_dsa.pub</tt>.
If you do not have any yet, generate it using the <code>ssh-keygen</code> command.</p>

<p>Please enter your username below so we can send you an authorization code to the e-mail address you gave us when you registered the account.</p>

<form method="post">
<p>Login: <input type="text" name="name" /></p>
<p style="display:none">Anti-captcha (leave empty!): <input type="text" name="mail" /></p>
<p><input type="submit" value="Send authorization code" /></p>
</form>
EOT
