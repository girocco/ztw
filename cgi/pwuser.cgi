#!/usr/bin/perl
# (c) Zachary T Welch <zw@superlucidity.net>
# (c) Petr Baudis <pasky@suse.cz>
# GPLv2

use strict;
use warnings;

use lib qw(/home/repo/repomgr/cgi);
use Git::RepoCGI;

my $repo = Git::RepoCGI->new('Forgotten User Password');

my $name = $repo->sparam('name');
my $user = Git::RepoCGI::User->load($name);
my $mail = $user->{email};

if ($repo->sparam('y0')) {
	# submitted
	my $newpwd = genpwd();

	my $domain = $repo->site_domain;
	my $subject = "[$domain] New password for user $name";
	open (M, '|-', 'mail', '-s', $subject, $mail)
		or $repo->bye("Cannot spawn mail: $!");
	print M <<EOT;
Hello,

Somebody requested the password for user '$name' to be reset,
so you are receiving the new password:

	$newpwd

If you did not request the password to be reset, then we apologize.
In any event, you must now use this new password to change the settings
for the '$name' account.

If you would like to change your password again, you can use this quick
link to your account settings page:

	http://$domain/m/edituser.cgi?name=$name

Have fun!
EOT
	close M or $repo->bye("Sending mail to $mail for $name failed: $!");

	$user->update_password($newpwd);

	print "<p>Account password has been reset. Have a nice day.</p>\n";
	exit;
}

print <<EOT;
<p>
If you have forgotten the password for your user account,
it can be reset to a random string that will be mailed to your
registered e-mail address.
</p>
<form method="post">
<input type="text" name="name" value="$name" />
<p><input type="submit" name="y0" value="Reset Password" /></p>
</form>
EOT
