#!/usr/bin/perl
# (c) Zachary T Welch <zw@superlucidity.net>
# GPLv2

use strict;
use warnings;

use lib qw(/home/repo/repomgr/cgi);
use Git::RepoCGI;

my $repo = Git::RepoCGI->new('Delete Watchdog');
my $project = $repo->sparam('project');
my $branch = $repo->sparam('branch');
my $path = $repo->sparam('path');
my $name = $repo->sparam('name');

# check for submission
my $ready = $project && $name;
$repo->bye('Not yet implemented') if $ready && $repo->sparam('go');

print <<EOT;
This form allows you to delete a watchdog for a project:
<form method="post">
<p>Project: <input type="text" name="project" value="$project" /></p>
<p>Branch: <input type="text" name="branch" value="$branch" /></p>
<p>Path: <input type="text" name="path" value="$path" /></p>
<br>
<p>Account: <input type="text" name="name" value="$name" /></p>
<h3>Authentication</h3>
To delete a watchdog.
You must also provide the project admin or user account password.
<p>Password: <input type="password" name="cpwd" /></p>
<p><input type="submit" name="go" value="Watch" /></p>
</form>
</form>
EOT
