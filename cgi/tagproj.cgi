#!/usr/bin/perl
# (c) Petr Baudis <pasky@suse.cz>
# GPLv2

use strict;
use warnings;

use lib qw(/home/repo/repomgr/cgi);
use Git::RepoCGI;
use CGI;

our $cgi = CGI->new;

my $pname = $cgi->param('p');
my $ctags = $cgi->param('t');

my $proj = Git::RepoCGI::Project->load($pname);
if (not $proj) {
	print $cgi->header(-status=>404);
	print "Project $pname does not exist.";
	exit;
}

if ($ctags =~ /[^ a-zA-Z0-9:.+#_-]/) {
	print $cgi->header(-status=>403);
	print "Content tag(s) '$ctags' contain evil characters.";
	exit;
}

foreach my $ctag (split(/ /, $ctags)) {
	# Locking is not important
	my $val = 0;
	open CT, $proj->{path}."/ctags/$ctag" and $val = <CT> and close CT;
	chomp $val;
	open CT, '>'.$proj->{path}."/ctags/$ctag" and print CT ($val+1)."\n" and close CT;
}

print $cgi->header(-status=>303, -location=>"/w/$pname.git");
