#!/usr/bin/perl
# (c) Petr Baudis <pasky@suse.cz>
# GPLv2

use strict;
use warnings;

use lib qw(/home/repo/repomgr/cgi);
use Git::RepoCGI;

my $captcha_question = "What is the name of Earth's nearest star?";
my $captcha_answer = 'sun';

my $repo = Git::RepoCGI->new('Project Registration');
my $name = $repo->sparam('name');

my $mode = $repo->sparam('mode');
if ($name and $mode =~ /^(mirror|push)$/) {
	# we permit the name to include '.git' suffix, but strip it off
	my $name = $repo->wparam('name');
	$name = $1 if $name =~ /(.*)\.git$/;

	# for local forks, the name must be prefixed with origin
	my $origin = $repo->wparam('origin');
	$name = "$origin/$name" if $origin;

	# FIXME: racy, do a lock
	valid_proj_name($name)
		and Git::RepoCGI::Project::does_exist($name)
		and $repo->err("Project with the name '$name' already exists.");

	if (lc($repo->sparam('mail')) ne lc('sun')) {
		print "<p>Sorry, invalid captcha check.</p>";
		exit;
	}

	my $mirror = $mode eq 'mirror';
	my $proj = Git::RepoCGI::Project->ghost($name, $mirror);
	if ($proj->cgi_fill($repo)) {
		if ($mirror) {
			$proj->premirror;
			print "<p>Initiated mirroring. You will be notified about the result by mail.</p>\n";
		} else {
			$proj->conjure;
			my $push_url = $proj->push_url;
			print <<EOT;
<p>The project <a href="/w/$name.git">$name</a> was created successfuly.</p>
<p>The push URL for your project is <tt>$push_url</tt>.</p>

<p>To push changes, you must add yourself as a user first!</p> You must
<a href="reguser.cgi">register as a user</a> and provide a SSH key to
use for authenticating access to the server.  A user may be given push
access to many projects, so other users may be given push access to your
project.  You can <a href="editproj.cgi?name=$name">assign users</a>
now, supplying the admin password when submitting your changes.</p>

<p>You cannot clone the repository until you push from an existing local
repository to create one or more branch.</p>

<p>If you experience permission problems trying to push, make sure that
your user account has permission.  A server script fixes some problems
periodically; if you get errors immediately after creating your project,
you may need to wait for its magic.</p>

<p>Enjoy your new project!</p>
EOT
		}
		exit;
	}
}

my $origin = $repo->wparam('origin');
# if not specified, derive the project fork origin from its name
if (!$origin && $name =~ m{^(.*)/(.*?)(\.git)?$}) {
	$origin = $1;
	# user should edit only the final portion of specified name.
	$name = $2;
}
$origin ||= '';

# if given, the origin must specify an existing project
if ($origin) {
	valid_proj_name($origin)
		&& Git::RepoCGI::Project::does_exist($origin)
		or $repo->err("No project with the name '$origin' exists.");
	$origin = undef;
}

# enter fork mode when told explicitly or when we find an origin project
my $is_fork = $mode =~ /^fork$/ || $origin;
# if called in fork mode and origin project is missing, request it
if ($is_fork && !$origin) {
	print <<EOT;
<form method="post">
<p>
Create a fork of <input type="text" name="origin" value="$name" />
<input type="submit" value="Continue" />
</p>
</form>
EOT
	exit
}

my $example = $origin || 'repo';
print <<EOT;
<p>This page allows you to register a new project.</p>

<h2>Project E-mail and Password</h2>

<p>When creating a project, a password must be
provided in order to later <a href="editproj.cgi?name=$example">adjust
its settings</a>.  If your admin password is lost, the
<a href="pwproj.cgi?name=$example">reset form</a></p> will produce
a new one and send it to the contact e-mail for the project.</p>
EOT

unless ($origin) {
	print <<EOT;
<h2>Hosting Modes</h2>

<p>Projects can be hosted in one of two modes: <i>mirror</i> or
<i>depot</i>.</p>

<p>You currently cannot switch freely between these two modes; if you
want to switch from mirroring to depot mode, the project administrator
may delete and recreate the project.  To switch the other way, you will
need to contact the site administrator.</p>

<h3>Mirror Mode</h3>

<p>In mirror mode, our dedicated git monkeys will check another
repository at a given URL every hour and mirror any new updates.</p>

<h3>Depot Mode</h3>

<p>In depot mode, <a href="reguser.cgi">registered users</a> can be
given permission to push changes to the repository.</p>

<h2>Importing an Existing Project</h2>

<p>If you want to develop a fork of another project hosted on this site,
this form should <b>not</b> be used.  Instead, go to that project's
gitweb page, and click the 'fork' link in the top bar.  This will save
hosting and user bandwidth, and -- more importantly -- your fork will
be properly categorized on the project's summary page.</p>

<p>If your project is a fork of an existing project that has not been
registered here, you should register a mirror for it first, then create
a fork from the mirror project.  You do not have to be involved in the
upstream project to register a mirror of it here, though you can contact
its maintainers to make them aware of this new resource.  A hosted mirror
provides an efficient solution for producing numerous forks of the project,
which may help attract new contributors to the project.</p>

<p>Remember, the distributed workflow available allowed by git means
that all team development proceeds in forks.  Thus, forks usually have
positive social consequence and help a project fully distribute its
development efforts.</p>

<p>So, go forth and fork!  Fork it all!  Fork long, and prosper!!</p>

<h2>Create A Project</h2>
EOT
} else {
	print <<EOT;
<h2>Create A Fork</h2>
<p>You are about to create a fork of the '$origin.git' project.</p>

<p>This means that it will be properly displayed as a subproject,
This process saves bandwidth (and time) because the original repository
contents become available automagically when pushing your branches.</p>
You will need to push only the data that <em>you</em> created.

<p>You do not need to specify any extra arguments during the push.
Specifically, you should <b>not</b> use <code>push --mirror</code> in
forked projects!  If you have push permissions for its parent (the
forkee) project, then this operation will erase <i>its</i>
<tt>refs/</tt> hierarchy.  This would upset its users considerably, so
this action should be avoided until a proper solution has been
developed and deployed.</p>

EOT
}
print <<EOT;
<form method="post">
<input type="hidden" name="origin" value="$origin" />
<p>Project name: <b>$origin/</b><input type="text" name="name" value="$name" /></p>
<p>Admin password: <input type="password" name="pwd" /></p>
<p>Admin password (retype): <input type="password" name="pwd2" /></p>
<p>E-mail contact: <input type="text" name="email" /></p>
EOT

if ($origin) {
	print <<EOT;
<input type="hidden" name="mode" value="push" />
<input type="hidden" name="origin" value="$origin" />
EOT
} else {
	print <<EOT;
<p>Hosting mode:</p><ul>
<li><input type="radio" name="mode" value="push" />Depot</li></ul>
<li><input type="radio" name="mode" value="mirror" />Mirror
URL: <input type="text" name="url" /> (e.g. git://example.com/foo/bar.git)</li>
<li><input type="radio" name="mode" value="fork" />Fork of
<input type="text" name="origin" value="$origin" />
(e.g. <a href="/w/repo.git"><tt>repo</tt></a> forks <tt>repo.git<tt>)</li>
EOT
}

print <<EOT;
<p>Description: <input type="text" name="desc" /></p>
<p>Homepage URL: <input type="text" name="hp" /></p>
<p>README (HTML, lt 8kb): <textarea name="README" rows="5" cols="80"></textarea></p>
<p>$captcha_question <input type="text" name="mail" /></p>

<p>By submitting this form, you are confirming that the repository
contains only free software and redistributing it does not violate any
law of Czech Republic. Read <a href="/about.html">more details</a> about
the hosting and terms and conditions.</p>

<p><input type="submit" name="y0" value="Register" /></p>
</form>
EOT
