#!/usr/bin/perl
# (c) Zachary T Welch <zw@superlucidity.net>
# GPLv2

use strict;
use warnings;

use lib qw(/home/repo/repomgr/cgi);
use Git::RepoCGI;

my $repo = Git::RepoCGI->new('Manage Watchdogs');
my $project = $repo->sparam('project');
my $branch = $repo->sparam('branch');
my $path = $repo->sparam('path');
my $user = $repo->sparam('user');

# check for submission
$repo->bye('Not yet implemented') if $repo->sparam('go');

# check for mutually exclusive parameters
$repo->bye("You may view either a project or a user, not both.")
	if $project && $user;

# obtain minimal parameters
unless ($project or $user) {
	print <<EOT;
<p>
To view watchdogs you must select a project or user.
</p>

<h3>View Project Watchdogs</h3>
<form method="post">
<p>Project: <input type="text" name="project" /></p>
<p>Branch: <input type="text" name="branch" value="$branch" /></p>
<p>Path: <input type="text" name="path" value="$path" /></p>
<p><input type="submit" value="View" /></p>
</form>

<h3>View User Watchdogs</h3>
<form method="post">
<p>Account: <input type="text" name="user" /></p>
<p><input type="submit" value="View" /></p>
</form>
EOT
	exit
}

# print on-line documentation
print <<EOT;
<p>
A watchdog set with empty branch and path fields will trigger for all
changes pushed to the repository.  Regular expressions can be used in
these fields to restrict notifications to matching branch or tree paths.
</p>
<p>
When activity occurs in a project, users are sent notification when the
fields of one or more watchdog are triggered.  These notification events
are aggregated for all projects and deliveried periodically via e-mail.
</p>
<p>
A single notification will be produced for each triggiering event, so
watchdogs using filter expressions that overlap do <em>not</em> generate
duplicate noticiations.  All watchdog filters that match will be listed
as a source the of an activity's notification, so you can update your
settings to eliminate duplicate filters.
</p>
<p>
The number of filters on the system directly impacts the notification
period, so you should try to cull redundant filters from these lists.
</p>
EOT

# set up for display user or project specific parts of the page
my %for_user = ( title => 'Account', key => 'user', value => $user,
	auth => 'User', no_dogs => "by $user",
	url_args => "name=$user" );
my %for_proj = ( title => 'Project', key => 'project', value => $project,
	auth => 'Admin', no_dogs => "to monitor $project",
	url_args => "project=$project&branch=$branch&path=$path");
my ( %showing, %listing );
if ($user) {
	%showing = %for_user;
	%listing = %for_proj;
} else {
	%showing = %for_proj;
	%listing = %for_user;
	print <<EOT;
<h3>Contacting Watchers</h3>
<p>
If you have push access to a project, you can send messages to
registered watchers of a particular fork, branch, or path by clicking on
the 'discuss' link.
</p>
EOT
}

# display section
print "<h3>Watchdogs for $showing{title} $showing{value}</h3>\n";

my @dogs = Git::RepoCGI::load(type => $showing{key}, name => $showing{value});
unless (@dogs) {
	my $no_dogs = <<EOT;
No watchdogs have been created $showing{no_dogs}.  Do you want to
<a href="watchadd.cgi?$showing{url_args}">create one</a>?
EOT
	$repo->bye($no_dogs);
}

print <<EOT;
<form method="post">
<table>
<tr>
<th>$listing{title}</th>
<th>Branch</th>
<th>Path</th>
<th>Contact</th>
<th>Delete</th>
</tr>
EOT

for my $dog ( @dogs ) {
	my ( $u, $r, $b, $p ) = ( '', '', '', '' ); #TODO: @$dog;
	my $key = "$u:$r:$b:$p";
	my $url_value = $user ? $r : $u;
	my $url_args = $user ? "project=$r.git&branch=$b&path=$p" : "name=$u";
	my $url = "<a href=\"watchedit.cgi?$url_args\">$url_value</a>";
	print <<EOT;
<tr>
<td>$url</td>
<td><input type="text" name="b" value="$b" /></td>
<td><input type="text" name="p" value="$p" /></td>
<td><a href="watchspam.cgi?project=$r&branch=$b&path=$p">contact</a>
<td><a href="watchdel.cgi?name=$u&project=$r&branch=$b&path=$p">delete</a>
</tr>
EOT
}

print <<EOT
</table>
<h3>$showing{auth} Authentication</h3>
<p>$showing{title}: <em>$showing{value}</em>
	<input type="hidden" name="$showing{key}" value="$showing{value}" /></p>
<p>Password: <input type="password" name="cpwd" /></p>
<p><input type="submit" name="go" value="Watch" /></p>
</form>
EOT
