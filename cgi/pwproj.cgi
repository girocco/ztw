#!/usr/bin/perl
# (c) Zachary T Welch <zw@superlucidity.net>
# (c) Petr Baudis <pasky@suse.cz>
# GPLv2

use strict;
use warnings;

use lib qw(/home/repo/repomgr/cgi);
use Git::RepoCGI;

my $repo = Git::RepoCGI->new('Forgotten Project Password');
my $name = $repo->sparam('name');
my $proj = $repo->load_project($name);

my $mail = $proj->{email};

if ($repo->sparam('y0')) {
	# submitted
	my $newpwd = genpwd();

	my $domain = $repo->site_domain;
	my $subject = "[$domain] New password for project $name";
	open (M, '|-', 'mail', '-s', $subject, $mail)
		or $repo->bye("Cannot spawn mail: $!");
	print M <<EOT;
Hello,

Somebody requested the password for project '$name.git' to be reset.

This address has been listed as the contact address for the project,
so you are receiving the new password:

	$newpwd

If you did not request the password to be reset, then we apologize.
In any event, you must now use this new password to change the settings
for the '$name.git' project.

If you would like to change your project password again, you can use
this quick link to edit the project page:

Quick-link to the edit project page:

	http://$domain/m/editproj.cgi?name=$name

Have fun!
EOT
	close M or $repo->bye("Sending mail to $mail for $name failed: $!");

	$proj->update_password($newpwd);

	print "<p>Project password has been reset. Have a nice day.</p>\n";
	exit;
}

print <<EOT;
<p>If you have forgotten the admin password for the '$name' project,
it can be reset to a random string that will be mailed to the listed
contact e-mail address:&nbsp;<tt>&lt;$mail&gt;</tt>.</p>
<form method="post">
<input type="hidden" name="name" value="$name" />
<p><input type="submit" name="y0" value="Reset Password" /></p>
</form>
EOT
