#!/usr/bin/perl

use strict;
use warnings;

my $home_path = '/data/work/repo.or.cz/site';
my @config = (
		site_admin => 'pasky@ucw.cz',
		site_domain => 'repo.or.cz',

		web_path => "$home_path/www",
		log_path => "/var/log/apache2",

		bin_path => "$home_path/bin",
		jail_path => "$home_path/jail",
		repomgr_path => "$home_path/repomgr",
		repodata_path => "$home_path/repodata",
		repo_path => "$home_path/repos",

		group_file => '/etc/group',
		user_file => '/etc/passwd',
		sshkeys_path => '/etc/sshkeys',

		doghouse_path => '/etc/watchdogs',
	);
my @keys = map { $config[$_ * 2] } 0 .. (@config / 2 - 1);

my $config = 'config.txt';
my $update = @ARGV && $ARGV[0] eq '-u';

sub read_config {
	open F, "<$config" or die $!;
	my %config;
	for (<F>) {
		chomp;
		next if /^\s*$/ || /^#/;
		my ( $key, $value ) = split(/=/, $_);
		$config{$key} = $value;
	}
	close F or die $!;
	%config
}

my %config = @config;
my $settings = read_config if -f $config;

if ($update || ! -f $config ) {
	open F, ">$config" or die $!;
	print F "# repository manager site parameters\n";
	print F $_, "=", $config{$_}, "\n" for @keys;
	close F or die $!;
	exit
}
read_config;

sub substitute {
	my $line = shift;
	$line =~ s/\@$_\@/$config{$_}/ for @keys;
	$line
}

while (<STDIN>) { print STDOUT substitute($_); }

