
SRCS = $(wildcard *.in)
SRCS += cgi/Git/RepoCGI/Config.pm.in
TARGETS = $(SRCS:%.in=%)

all: $(TARGETS) config.txt

$(TARGETS): %: %.in config.txt
	./config.pl < $< > $@
	chmod +x $@

config.txt: config.pl
	./config.pl -u
	touch $(SRCS)

clean: $(TARGETS)
	rm -f $^

